import { AppState } from "./../../app.module";
import { Store } from "@ngrx/store";
import { DestinoViaje } from "../../models/destino-viaje.model";
import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { DestinosApiClient } from "src/app/models/destinos-api-client.model";

@Component({
  selector: "app-lista-destinos",
  templateUrl: "./lista-destinos.component.html",
  styleUrls: ["./lista-destinos.component.css"],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;
  constructor(
    public destinosApliClient: DestinosApiClient,
    private store: Store<AppState>
  ) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store
      .select(state => state.destinos.favorito)
      .subscribe(d => {
        if (d != null) {
          this.updates.push(`Se ha elegido a ${d.nombre}`);
        }
      });
      store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit() {}

  agregar(d: DestinoViaje) {
    this.destinosApliClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(d: DestinoViaje) {
    this.destinosApliClient.elegir(d);
  }

  getAll(){}
}
