import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Inject,
  forwardRef
} from "@angular/core";
import { DestinoViaje } from "src/app/models/destino-viaje.model";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ValidatorFn
} from "@angular/forms";
import { fromEvent } from "rxjs";
import { ajax, AjaxResponse } from "rxjs/ajax";
import { AppConfig, APP_CONFIG } from "src/app/app.module";
import {
  map,
  filter,
  debounceTime,
  distinctUntilChanged,
  switchMap
} from "rxjs/operators";

@Component({
  selector: "app-form-destino-viaje",
  templateUrl: "./form-destino-viaje.component.html",
  styleUrls: ["./form-destino-viaje.component.css"]
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 3;
  searchResults: string[];
  constructor(
    fb: FormBuilder,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig
  ) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: [
        "",
        [
          Validators.required,
          this.nombreValidador,
          this.nombreValidadorParametrizado(this.minLongitud)
        ]
      ],
      url: ["", Validators.required]
    });
  }

  ngOnInit() {
    let elemHtml = <HTMLInputElement>document.getElementById("nombre");
    fromEvent(elemHtml, "input")
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 4),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap((text: string) =>
          ajax(this.config.apiEndpoint + "/ciudades?q=" + text)
        )
      )
      .subscribe(ajaxResponse => (this.searchResults = ajaxResponse.response));
  }

  guardar(nombre: string, url: string): boolean {
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    //console.log(d);
    return false;
  }

  nombreValidador(comp: FormControl): { [key: string]: boolean } {
    const l = comp.value.toString().trim().length;
    if (l > 0 && l < 5) {
      return { invalidName: true };
    }
    return null;
  }
  nombreValidadorParametrizado(minLong: number): ValidatorFn {
    return (comp: FormControl): { [key: string]: boolean } => {
      const l = comp.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { minLongName: true };
      }
      return null;
    };
  }
}
