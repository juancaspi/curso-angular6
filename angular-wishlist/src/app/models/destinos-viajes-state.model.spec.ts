import {
  reducerDestinosViajes,
  DestinosViajesState,
  intializeDestinosViajesState,
  InitMyDataAction,
  NuevoDestinoAction,
  ElegidoFavoritoAction,
  VoteUpAction,
  VoteDownAction,
  VoteResetAction
} from './destinos-viajes-state.model';
import { DestinoViaje } from './destino-viaje.model';
import { BorradoDestinoAction } from './destinos-viajes-state.model';

describe('reducerDestinosViajes', () => {
  it('should reduce init data', () => {
    //setup
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
    // action
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    //assertions
    expect(newState.items.length).toEqual(2);
    expect(newState.items[0].nombre).toEqual('destino 1');
  });

  it('should reduce new item added', () => {
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].nombre).toEqual('barcelona');
  });

  it('should reduce vote up item', () => {
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const action: VoteUpAction = new VoteUpAction(new DestinoViaje('barcelona', 'url'));
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    //console.log(action.destino.votes);
    expect(action.destino.votes).toEqual(1);
  });

  it('should reduce vote down item', () => {
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const action: VoteDownAction = new VoteDownAction(new DestinoViaje('barcelona', 'url'));
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    //console.log(action.destino.votes);
    expect(action.destino.votes).toEqual(-1);
  });

  it('should reduce reset vote item', () => {
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    let d = new DestinoViaje('barcelona', 'url');
    d.votes = 5;
    const action: VoteResetAction = new VoteResetAction(d);
    //console.log(action.destino.votes);
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    //console.log(action.destino.votes);
    expect(action.destino.votes).toEqual(0);
  });

  it('should reduce delete item', () => {
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const addAction: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona2', 'url'));    
    const addAction2: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
    let addState: DestinosViajesState = reducerDestinosViajes(prevState, addAction);
    addState = reducerDestinosViajes(prevState, addAction2);

    const action: BorradoDestinoAction = new BorradoDestinoAction(addAction.destino);
    const newState = reducerDestinosViajes(addState, action);
    expect(newState.items.length).toEqual(1);
  });

  it('should reduce favorite item', () => {
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const addAction: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona2', 'url'));    
    const addAction2: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
    let addState: DestinosViajesState = reducerDestinosViajes(prevState, addAction);
    addState = reducerDestinosViajes(prevState, addAction2);

    const action: ElegidoFavoritoAction = new ElegidoFavoritoAction(addAction.destino);
    const newState = reducerDestinosViajes(addState, action);
    expect(newState.favorito.nombre).toEqual('barcelona2');
  });
});