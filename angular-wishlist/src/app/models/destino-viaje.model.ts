import {v4 as uuid} from 'uuid';
export class DestinoViaje {
  id = uuid();  
  public votes = 0;
  private selected: boolean;
  public servicios: string[];
  constructor(public nombre: string, public url: string) {
    this.servicios = ["pileta", "desayuno"];
  }
  isSelected(): boolean {
    return this.selected;
  }
  setSelected(s: boolean) {
    this.selected = s;
  }
  voteUp(): any {
    this.votes++;
  }
  voteDown(): any {
    this.votes--;
  }

  voteReset(): any{
    this.votes = 0;
  }
}
